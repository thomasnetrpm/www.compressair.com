module.exports = function(grunt) {

grunt.initConfig({
	pkg: grunt.file.readJSON('package.json'),
	uglify: {
		my_target: {
			files: {
			'js/production.min.js': ['js/plugins.js', 'js/main.js']
			}
		}
	},
	watch: {
		options: {
	        livereload: true,
	    },
	    scripts: {
	        files: ['js/*.js'],
	        tasks: ['uglify'],
	        options: {
	            spawn: false,
	        },
	    },
	    css: {
		    files: ['*.scss','css/**/*.scss','css/**/**/*.scss'],
		    tasks: ['sass'],
		    options: {
		        spawn: false,
		    }
		}, 
		html: {
            files: ['index.html'],
        }
	},
	imageoptim: {
		myTask: {
			src: ['img']
		}
	},
	sass: {
	    dist: {
	        options: {
	            style: 'compressed'
	        },
	        files: {
	            'style.css': 'style.scss'
	        }
	    } 
	},
	grunticon: {
	    myIcons: {
	        files: [{
	            expand: true,
	            cwd: 'img',
	            src: ['*.svg', '*.png'],
	            dest: "img-output"
	        }],
	        options: {
	        }
	    }
	}
});

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-imageoptim');
    grunt.loadNpmTasks('grunt-contrib-sass');
    //grunt.loadNpmTasks('grunt-grunticon');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['uglify','imageoptim','sass',/*'grunticon:myIcons',*/'watch']);

};