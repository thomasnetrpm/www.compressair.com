<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<!--Site Content-->
	<section class="site-content clearfix" role="main">
	    <div class="inner-wrap">
	        <article class="site-content-primary clearfix"> 
<!--<?php if(get_field('live_url')): ?>
<a href="<?php the_field('live_url'); ?>" target="_blank" class="blue-btn live-url">Live URL</a>
<?php endif; ?>-->
    <h1>
<?php
if(get_field('alternative_h1'))
{
	echo get_field('alternative_h1');
}
 else 
{
  the_title();
}
?></h1>  
				<!--<figure class="product-intro-img  col-3of9">
				<a href="<?php bloginfo('template_url'); ?>/img/featured-img.jpg" class="lightbox"><img src="<?php bloginfo('template_url'); ?>/img/featured-img.jpg" alt="Featured Image"></a>
				<figcaption>
				<a href="#" class="blue-btn-m product-intro-cta">View Catalog</a>
				</figcaption>
				</figure>-->





<?php if ( has_post_thumbnail()) : ?>
		<figure class="product-intro-img">
			<?php if ( has_post_thumbnail()) {
   $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');
   echo '<a href="' . $large_image_url[0] . '"  class="lightbox" >';
   echo get_the_post_thumbnail($post->ID, 'medium'); 
   echo '</a>';
}
?>


<?php if( have_rows('additional_product_img') ):		
				while ( have_rows('additional_product_img') ) : the_row(); ?>
<?php 	
	$image = wp_get_attachment_image_src(get_sub_field('product_img'), 'thumbnail');
	$imagelarge = wp_get_attachment_image_src(get_sub_field('product_img'), 'full');
 ?>
 <a href="<?php echo $imagelarge[0]; ?>" class="lightbox product-intro-img-additional">
 <img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
 </a>
<?php endwhile; ?>
<?php endif; ?>

			
			

<?php if(get_field('aside_cta') ): ?>
	<figcaption>
		<?php the_field('aside_cta'); ?>
	</figcaption>
<?php endif; ?>

			
		</figure>
	<?php endif; ?>




<?php if(get_field('aside_cta') && has_post_thumbnail() == false  ): ?>
<div class="cta-aside">
<?php the_field('aside_cta'); ?>
</div>      
<?php endif; ?>
	       		<?php the_content(); ?> 
				<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>
				<?php if (is_page( '9' )) : ?>
					<!--Sitemap Page-->
				    <ul>
				    <?php
				    // Add pages you'd like to exclude in the exclude here
				    wp_list_pages(
				    array(
				    'exclude' => '',
				    'title_li' => '',
				    )
				    );
				    ?>
				    </ul>
				<?php endif; ?>                    
	        </article>
	        <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar' ) ); ?>
		</div>
	</section>

<?php endwhile; ?>

<?php if(get_field('slide_cta') ): ?>
	 <p id="last"></p>
           <div id="slidebox"><a class="close">&nbsp;</a>
          <?php the_field('slide_cta'); ?>
<!-- end HubSpot Call-to-Action Code -->
</div>
		
<?php endif; ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>