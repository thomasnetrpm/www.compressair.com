<!--Secondary Content-->
                <aside class="site-content-secondary">
                    
                    <nav class="aside-nav">
                        <a href="#" class="activate-link aside-header"><h3>Air Compressors</h3></a>
                        <?php wp_nav_menu(array('menu' => 'Air Compressors','container' => '','items_wrap'      => '<ul class="aside-nav-ul">%3$s</ul>',)); ?>
                            <a href="#" class="activate-link aside-header"><h3>Miscellaneous Equipment</h3></a>
                        <?php wp_nav_menu(array('menu' => 'Miscellaneous Equipment','container' => '','items_wrap'      => '<ul class="aside-nav-ul">%3$s</ul>',)); ?>
                       
                    </nav>
                    <a href="http://www.compressedairsystems.com/financing-and-leasing-options.html" class="blue-btn aside-cta">Financing Available</a>
                </aside>
            <div class="fauxcolumn"></div>



<!--Secondary Content
<aside class="site-content-secondary col-3">

<?php if (is_page( '20' ) || '20' == $post->post_parent) : ?>
      
    <h3 class="nav-aside-header">Expanded Metal</h3>
    <nav class="nav-aside">
         <?php wp_nav_menu(array('menu' => 'Expanded Metal','container' => '','items_wrap'      => '<ul>%3$s</ul>',)); ?>        
    </nav>
    
<?php elseif (is_page( 'services' ) || '14' == $post->post_parent) : ?>

<?php endif; ?>

     <?php if(get_field('aside_cta') ): ?>
      <div class="cta-aside">
        <?php the_field('aside_cta'); ?>
     </div>   
    
                   
    <?php endif; ?>
    
<?php if(get_field('additional_aside_content') ): ?>
		<?php the_field('additional_aside_content'); ?>
<?php endif; ?>
</aside>


-->