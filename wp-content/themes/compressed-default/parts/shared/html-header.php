<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<title><?php wp_title( '|' ); ?></title>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.ico"/>
		<?php wp_head(); ?>
		 <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500' rel='stylesheet' type='text/css'>
         <link href='http://fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
		<!--[if lt IE 9]>
            <script src="<?php bloginfo('template_url'); ?>/js/vendor/respond.min.js"></script> <script src="<?php bloginfo('template_url'); ?>/js/vendor/selectivizr-min.js"></script>
        <![endif]-->
        <script type="text/javascript">
         (function(i, s, o, g, r, a, m) {
             i['GoogleAnalyticsObject'] = r;
             i[r] = i[r] ||
             function() {
                 (i[r].q = i[r].q || []).push(arguments)
             }, i[r].l = 1 * new Date();
             a = s.createElement(o), m = s.getElementsByTagName(o)[0];
             a.async = 1;
             a.src = g;
             m.parentNode.insertBefore(a, m)
         })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
         ga('create', 'UA-46955727-1', 'compressedairsystems.com');
         ga('send', 'pageview');
      </script>
    <script type="text/javascript">
        function ctlSearch_OnClick ()  
        {  
         var url;  
         var theform = document.frmSearchBox;  
           
         if (theform.keyword.value.length < 2)  
         {  
          alert('Please enter at least two characters.');  
          theform.keyword.focus();  
         }  
         else  
         {   var iIndex = theform.ddlSearchType.value;  
           var sSearchType = theform.ddlSearchType.value;  
            
        // Version 2.7 release  
        // 1. using default Domain, like "orientalmotor.thomasnet.com" in our sample   
        // 2. using root category shortName, like "all-categories" in our sample  
          url = "http://compressedair.stage.thomasnet-navigator.com/keyword/?&plpver=1001&key=all&keycateg=100"  
          url = url + "&SchType=" + sSearchType;  
          url = url + "&keyword=" + encodeURI(theform.keyword.value);  
          url = url + "&refer=" + encodeURI("http://" + document.location.hostname);     
          document.location.href = url;  
         }  
        //alert (url);              
        } 
    </script>

      <script type="text/JavaScript" src="https://secure.ifbyphone.com/js/ibp_clickto_referral.js"></script>
      <script type="text/JavaScript">
         var _ibp_public_key = "1f634e9d5b4adf9c229c909dbf5fd1041416df0a";
         var _ibp_formatting = true;
         var _ibp_keyword_set = 83084;
      </script>
	</head>
	<body <?php body_class(); ?>>
