<div class="page-wrap">

    <!--Site Header-->
    <header class="site-header" role="banner">
        
        <a href="<?php bloginfo('url'); ?>" class="site-logo"><img src="<?php bloginfo('template_url'); ?>/img/compressed-air-logo.jpg" alt="Compressed Air Logo"></a>
        
        <div class="utility-nav">
            <p class="site-tagline">Serving our <br> Customers for over 50 Years</p>
            <a class="site-ph" href="tel:800-626-8177"><span>Toll Free:</span> 800.626.8177</a>
            

            <a href="#menu" class="mobile-nav mobile menu-link active">
                <span>Menu Anchor</span>
            </a>
        </div>     
        <a href="http://catalog.compressedairsystems.com/request/all-categories" class="site-rfq red-btn">Request a Quote</a>
        <a href="http://aircompressors.compressedairsystems.com/blog" class="site-rfq blue-btn"><strong>Stay Informed,</strong> Read Our Blog</a>




<div class="google-search">
    <form class="search" action="http://catalog.compressedairsystems.com/keyword/?&amp;plpver=1001&amp;key=all&amp;keycateg=100" method="get"><input type="text" value="" title="Search Keyword" placeholder="Search Keyword" name="keyword" id="keyword" class="search-text"><input type="submit" value="Go" title="Submit" class="search-submit"></form>
</div>

            <nav id="menu" class="menu clearfix" role="navigation">
                <ul class="level-1">
                <li class="nav-0">
                        <form action="http://catalog.compressedairsystems.com/keyword/?&amp;plpver=1001&amp;key=all&amp;keycateg=100" method="get" class="search-form">
                            <div class="search-table">
                                <div class="search-row">
                                    
                                    <div class="search-cell1">
                                        <input type="text" id="search-site" value="" placeholder="Search Website..." name="s" class="search-text" title="Search Our Site">    
                                    </div>
                                    <div class="search-cell2">
                                        <input class="search-submit" alt="Search" title="Search" value="" type="submit">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </li>
                    <?php wp_nav_menu(array('menu' => 'Primary Nav','container' => '','items_wrap'      => '%3$s',)); ?> 

                    <li class="nav-9">
                        <div class="social-wrap ">
                            <a href="https://www.facebook.com/CAS1963" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-facebook.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-facebook.png" alt="Facebook"></a>
                            <a href="https://twitter.com/CompAirSystems" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-twitter.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-twitter.png" alt="Twitter"></a>
                            <a href="https://plus.google.com/u/0/+Compressedairsystems/posts" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-googleplus.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-googleplus.png" alt="Google Plus"></a>
                            <a href="https://www.linkedin.com/company/compressed-air-systems-inc-" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-linkedin.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-linkedin.png" alt="LinkedIn"></a>
                            <a href="https://www.youtube.com/channel/UCvBeoqWzNlu4x5P-hmiK0AA" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-youtube.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-youtube.png" alt="YouTube"></a>
                            <a href="https://cart.thomasnet-navigator.com/checkout/ViewCart.aspx?CID=1329&cartid=&plpver=1001&carttype=3&origin=Plp&sid=1408171342178900366399B-B&profile_id=0&lsid=51260154&plpurl=http://catalog.compressedairsystems.com/category/all-categories?&__utma=88460356.1427860549.1392907075.1408297337.1408302192.6&__utmb=88460356.6.9.1408302799774&__utmc=88460356&__utmx=-&__utmz=88460356.1408302192.6.5.utmcsr=compressedairsystems.com|utmccn=(referral)|utmcmd=referral|utmcct=/airblower-gardnerdenver.html&__utmv=-&__utmk=63357990" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-cart.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-cart.png" alt="cart"></a>
                        </div>
                    </li>
                    
                </ul>
            </nav>
    </header> 