<!--Site Footer-->
<footer class="site-footer" role="contentinfo">
    <div class="inner-wrap large-footer  dark-module">
        <div class="col-3">
            <h3>Services</h3>
<p>
<a href="http://www.compressedairsystems.com/aircompressorservice.html">Air Compressor Service</a><br>
<a href="http://www.compressedairsystems.com/air-compressor-rental.html">Air Compressor Rentals</a><br>
<a href="http://www.compressedairsystems.com/rotary-screw-airends-repair.html">Compressor Rotary Screw Rebuilds</a><br>
<a href="http://www.compressedairsystems.com/compressed-air-leak-detection.html">Compressed Air Leak Detection</a><br>
<a href="http://www.compressedairsystems.com/compressedairaudit.html">Compressed Air Audits</a>
</p>
        </div>
        <div class="col-3">
            <h3>Other Products Available</h3>
<p>
<a href="http://www.compressedairsystems.com/nitrogen-generators.html">Nitrogen Generators</a><br>
<a href="http://www.compressedairsystems.com/highpressure-aircompressor.html">High Pressure Air Compressors</a><br>
<a href="http://www.compressedairsystems.com/compressedairdryers.html">Compressed Air Dryers</a><br>
<a href="http://www.compressedairsystems.com/compressedairtreatment.html">Air Compressor Filtration</a><br>
<a href="http://www.compressedairsystems.com/compressor-oil.html">Aftermarket Air Compressor Lubricants</a>
</p>
        </div>
        <div class="col-3">
            <h3>P.O.G.O Railcar Gate Opener</h3>
<p>
<b><a href="http://www.compressedairsystems.com/pogo-flash-presentation.html">Watch POGO Video Now</a></b><br>
<a href="http://www.compressedairsystems.com/pogo.html">POGO Technical Data</a><br>
<a href="http://www.compressedairsystems.com/hopper-door-openers.html">POGO Information</a><br>
<a href="http://www.compressedairsystems.com/railcar-unloading.html">Railcar Unloading</a><br>
<a href="http://www.compressedairsystems.com/rail-car-openers.html">What is a POGO?</a>
</p>
        </div>
        <div class="col-3 col-last">
            <h3>Contact</h3>
            <p>
            Compressed Air Systems, Inc.<br>
            9303 Stannum Street<br>
            Tampa, FL 33619-2658<br>
            Toll Free: 800.626.8177 <br>
            Ph: 813.626.8177  |  F: 813.628.0187<br>
            <a href="mailto:info@compressedairsystems.com">info@compressedairsystems.com</a></p>
        </div>
    </div>
    <div class="inner-wrap smaller-footer">
        <p class="footer-copy">Copyright © 2014 Compressed Air Systems &nbsp;|&nbsp; Site Created by <a href="http://business.thomasnet.com/rpm" target="_blank">ThomasNet RPM</a></p>
        <p class="footer-site"><a href="<?php bloginfo('url'); ?>/company-profile.html">About</a> &nbsp;|&nbsp; <a href="http://aircompressors.compressedairsystems.com/contact-us-compressed-air-systems-inc" target="_blank">Contact</a> &nbsp;|&nbsp; <a href="<?php bloginfo('url'); ?>/privacy-policy.html">Privacy Policy</a> &nbsp;|&nbsp; <a href="<?php bloginfo('url'); ?>/terms-of-service.html">Terms of Service</a> &nbsp;|&nbsp; <a href="<?php bloginfo('url'); ?>/sitemap.html">Sitemap</a></p>
    </div>
</footer>

</div><!--end of inner-wrap-->