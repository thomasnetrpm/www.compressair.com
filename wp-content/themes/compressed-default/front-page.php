<?php

	/*
		Template Name: Front Page
	*/
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
 	<!--Site Intro-->
    <section class="site-intro">
        <div class="inner-wrap">
            <h1 class="site-intro-h1">
                <span class="br-medium-up">New &amp; Used</span> Industrial Air Compressors for Sale
            </h1>
            <p class="site-intro-text">
                Compressed Air Systems offers a huge selection of the best new and used air compressors for sale. We are also an authorized Kaeser compressor dealer, and carry many models of their reliable and durable new and used air compressors.
            </p>
        </div>
    </section>  
	<!--Site Content-->
	<section class="site-content clearfix" role="main">
	    <div class="inner-wrap">
	        <article class="site-content-primary clearfix">
	       		<?php the_content(); ?> 
				<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>                
	        </article>
	        <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar' ) ); ?>
		</div>
	</section>

<?php endwhile; ?>

<?php if(get_field('slide_cta') ): ?>
	 <p id="last"></p>
           <div id="slidebox"><a class="close">&nbsp;</a>
          <?php the_field('slide_cta'); ?>
<!-- end HubSpot Call-to-Action Code -->
</div>
		
<?php endif; ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>