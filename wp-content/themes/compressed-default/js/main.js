//PNG Fallback

if(!Modernizr.svg) {
  var images = $('img[data-png-fallback]');
  images.each(function(i) {
    $(this).attr('src', $(this).data('png-fallback'));
  });
}

//Toggle Boxes
$(document).ready(function() {
  $('body').addClass('js');
  var $activatelink = $('.activate-link');
  
$activatelink.click(function() {
	var $this = $(this);
	$this.toggleClass('active').next('div').toggleClass('active');
	return false;
});

});

//Responsive Navigation
$(document).ready(function() {
	  $('body').addClass('js');
	  var $menu = $('#menu'),
	  	  $menulink = $('.menu-link'),
	  	  $menuTrigger = $('.has-subnav > a');
	
	$menulink.click(function(e) {
		e.preventDefault();
		$menulink.toggleClass('active');
		$menu.toggleClass('active');
	});
	
	$menuTrigger.click(function(e) {
		e.preventDefault();
		var $this = $(this);
		$this.toggleClass('active').next('ul').toggleClass('active');
	});
	
});


//Lightbox
$(document).ready(function() {
  $('.lightbox').magnificPopup({type:'image'});
});
$(document).ready(function() {
	$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});
});



//Show More
$(document).ready(function() {
	$( ".showmore" ).after( "<p><a href='#' class='show-more-link'>More</a></p>");
	var $showmorelink = $('.showmore-link');  
	$showmorelink.click(function() {
		var $this = $(this);
		var $showmorecontent = $('.showmore');
		$this.toggleClass('active');
		$showmorecontent.toggleClass('active');
		return false;
	});
});

//Show More
$(document).ready(function() {
	var $expandlink = $('.headexpand');  
	$expandlink.click(function() {
		var $this = $(this);
		var $showmorecontent = $('.showmore');


		$this.toggleClass('active').next().toggleClass('active');
		$showmorecontent.toggleClass('active');
		return false;
	});
});

 
//Flexslider    
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false
  });
});


mediaCheck({
  media: '(min-width: 640px)',
  entry: function() {
   $(".site-intro > .inner-wrap").backstretch([
   		"http://127.0.0.1/compressed-air/wp-content/themes/compressed-default/img/site-intro-1.jpg",
   		"http://127.0.0.1/compressed-air/wp-content/themes/compressed-default/img/site-intro-2.jpg",
   		"http://127.0.0.1/compressed-air/wp-content/themes/compressed-default/img/site-intro-3.jpg",
   		"http://127.0.0.1/compressed-air/wp-content/themes/compressed-default/img/site-intro-4.jpg"
   		], {duration: 3000, fade:'slow'});
  },
  exit: function() {
    $(".site-intro > .inner-wrap").backstretch("destroy");
  },
  both: function() {
    console.log('changing state');
  }
});